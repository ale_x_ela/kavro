<?php namespace Mishai\Sitemanage\Models;

use Model;

/**
 * aboutus Model
 */
class Aboutus extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'mishai_sitemanage_aboutuses';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = ['home_bgs' => 'Mishai\Sitemanage\Models\Aboutus'];
    public $belongsTo = [];
    public $belongsToMany = [];

    public $video = [];
}
