<?php namespace Mishai\Sitemanage\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateAboutusesTable extends Migration
{
    public function up()
    {
        Schema::create('mishai_sitemanage_aboutuses', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('mishai_sitemanage_aboutuses');
    }
}
