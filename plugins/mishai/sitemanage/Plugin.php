<?php namespace Mishai\Sitemanage;

use Backend;
use System\Classes\PluginBase;

/**
 * sitemanage Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'sitemanage',
            'description' => 'No description provided yet...',
            'author'      => 'mishai',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Mishai\Sitemanage\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'mishai.sitemanage.some_permission' => [
                'tab' => 'sitemanage',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'sitemanage' => [
                'label'       => 'sitemanage',
                'url'         => Backend::url('mishai/sitemanage/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['mishai.sitemanage.*'],
                'order'       => 500,
            ],
        ];
    }
}
